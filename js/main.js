const ProductList = []; // Danh sach san pham
const ListCart = []; // Danh sach san pham gio hang

// Lay danh sach
const getListProduct = function () {
    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
        method: "GET",
    })
        .then(function (res) {
            console.log(res);
            let productlist = mapData(res.data);
            creatListProduct(ProductList);
            ProductList.push(productlist);
            console.log(ProductList);
        })
        .catch(function (error) {
            console.log(error);
        })
}

// Chuyen doi data backend thanh data cua minh va gan ProductList
const mapData = function (dataFromDB) {
    for (let i = 0; i < dataFromDB.length; i++) {
        let mappedProduct = new Product(
            dataFromDB[i].id,
            dataFromDB[i].name,
            dataFromDB[i].price,
            dataFromDB[i].screen,
            dataFromDB[i].backCamera,
            dataFromDB[i].fontCamera,
            dataFromDB[i].img,
            dataFromDB[i].desc,
            dataFromDB[i].type,
        );
        ProductList.push(mappedProduct);
    }
    return ProductList;
}

// Tao danh sach san pham
const creatListProduct = function (data) {
    let productHTML = "";
    for (let i = 0; i < data.length; i++) {
        productHTML += ` 
             <div class= "col-2">
                <div class="card w-200">
                    <img
                        style="height:280px; width: 100%"
                        src="${data[i].img}" 
                        alt="img_product">
                    <h4>${data[i].name}</h4>
                    <p>${data[i].type}</p>
                    <p>${data[i].price}</p>
                    <button class="btn" style="background-color: #f8462f; color:#fff;" onclick= "addProduct('${ProductList[i].id}')">Mua</button>
                </div>
            </div>
        </div>
        `
    }
    document.getElementById("listProduct").innerHTML = productHTML;
};

// function filter loai san pham
const filterStyle = function () {
    let filterlist = [];
    let getStyle = document.getElementById("selectStyle").value;

    if (getStyle === "Iphone") {
        for (let i = 0; i < ProductList.length; i++) {
            if (ProductList[i].type === "iphone") {
                filterlist.push(ProductList[i]);
                creatListProduct(filterlist);
            } else {
                console.log("Khong thanh cong !");
            }
        }
    } else if (getStyle === "SamSung") {
        for (let i = 0; i < ProductList.length; i++) {
            if (ProductList[i].type === "samsung") {
                filterlist.push(ProductList[i]);
                creatListProduct(filterlist);
            } else {
                console.log("Khong thanh cong !");
            }
        }
    } else {
        creatListProduct(ProductList);
    }
}
// function add card
const getProduct = function (id) {
    for (let i = 0; i < ProductList.length; i++) {
        if (ProductList[i].id === id) {
            return {
                Product: ProductList[i],
                quantity: 1,
            };
        }
    }
};

// function chon san pham
const addProduct = function (id) {
    const cartitem = getProduct(id);
    if (ListCart.length === 0) {
        ListCart.push(cartitem);
    }
    else {
        const index = ListCart.findIndex(function (item) { return item.Product.id == id });
        console.log('add', ListCart);
        if (index !== -1) {
            ListCart[index].quantity++;

        } else {
            ListCart.push(cartitem);
        }
        console.log(index);
    }

    renderCart();
    updateSumCart(cartitem)
    setLocalStorage();
}
// Show cart
const renderCart = function () {
    let cartHtlm = "";
    for (let i = 0; i < ListCart.length; i++) {
        cartHtlm += `
        <tr>
            <td><img style="width:150px;" src="${ListCart[i].Product.img}" /></td>
            <td>${ListCart[i].Product.name}</td>
            <td>${ListCart[i].Product.price}</td>
            <td>${ListCart[i].quantity}</td>
            <td>
                <div class="btn-group">
                    <button id="reduct" class="btn btn-info border-right" onclick="reductQuantity(${ListCart[i].Product.id})">-</button>
                    <button id="rise" class="btn btn-info border-left" onclick="riseQuantity(${ListCart[i].Product.id})">+</button>
                </div>
            </td>

             <td>${ListCart[i].Product.price * ListCart[i].quantity}</td >
            <td></td>
            <td>
                <button class="btn btn-info" onclick="handleDeleteProduct(${ListCart[i].Product.id})">x</button>
            </td>
        </tr > `;
    }
    document.getElementById("tbCart").innerHTML = cartHtlm;
};

/**
 * Xoa san pham trong gio hang
 */
// Tim san pham trong gio hang
const findProduct = function (id) {
    // console.log('findProduct', id);
    for (let i = 0; i < ListCart.length; i++) {
        // thằng nay đang sai. Nos đang log ra gia tri undefinded nên phải xem lại cái listCart này có chứa id k
        // nó phải lấy thêm 1 lần nua mới tơi được id
        // sao sai mentor
        console.log(ListCart[i].Product.id);
        if (ListCart[i].Product.id == id) {
            return i
        }
    }
    return -1;
}
// hàm xóa sản phẩm trong giỏ hàng
const handleDeleteProduct = function (id) {
    const index = findProduct(id);
    ListCart.splice(index, 1);
    renderCart(ListCart);
    setLocalStorage();
}
//Tăng quantity
const riseQuantity = function (id) {
    // console.log('Tăng quantity', id);
    let index = findProduct(id);
    console.log(index);
    if (index !== -1) {
        console.log(index);
        //xem lai đi nhé :D
        // Dạ em cảm ơn mentor
        // sẵn mentor xem dùm em là khi ấn nút mua nó ko tăng số lượng mà nó add thêm vào  mảng

        //i nay là ở đâu v =.=. sao lại là i đc nhỉ =)))
        // A mình quên đã tìm Product gan vào index
        //=))
        // là lấy vị trí Product trong mảng
        ListCart[index].quantity += 1;
        renderCart(ListCart);
        updateSumCart(ListCart[index]);
        setLocalStorage();
    }
}
// Giam quantity
const reductQuantity = function (id) {
    // console.log('Giam quantity', id);
    let index = findProduct(id);
    if (index !== -1) {
        if (ListCart[index].quantity > 1) {
            ListCart[index].quantity -= 1;
            renderCart(ListCart);
            setLocalStorage();
            return;
        }
    }
}
/**
 * Tính tổng tiền
 */
let sumCart = 0;
const updateSumCart = (cartitem) => {
    sumCart += +cartitem.Product.price;
    document.getElementById('totalMoney').innerHTML = sumCart;
}

/**
 * Luu vào localStorage
 */
const setLocalStorage = function () {
    const listcartJSON = JSON.stringify(ListCart);
    localStorage.setItem("ListCart", listcartJSON);
}
//Lấy từ LocalStorage
const getLocalStorage = function () {
    const dataJSON = localStorage.getItem("ListCart");
    console.log(dataJSON);
    if (dataJSON !== -1) {
        return;
    }
    const data = JSON.parse(dataJSON);
    ListCart = data;
    renderCart();
    setLocalStorage();
}

// Ham main chinh
const main = function () {
    getListProduct();
    console.log(ProductList);
    console.log(ListCart);
    getLocalStorage();
}
main();

